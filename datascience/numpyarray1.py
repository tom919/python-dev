import numpy as np 

np.random.seed(0) #seed for reproduce

x1 = np.random.randint(10, size=6)
print("x1")
print(x1)

x2 = np.random.randint(10, size=(3,4))
print("x2")
print(x2)

x3 = np.random.randint(10, size=(3,4,5))
print("x3")
print(x3)

x3_sub = x3[:2,:2, :2]
print("x3 sub 2 2")
print(x3_sub)