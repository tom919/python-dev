import numpy as np 

tetha = np.linspace(0, np.pi, 3)
print(tetha)
print("sin tetha = ", np.sin(tetha))
print("cos tetha = ", np.cos(tetha))
print("tan tetha = ", np.tan(tetha))