import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn; seaborn.set()

data = pd.read_csv("data/pres.csv")
heights = np.array(data['height(cm)'])
print(heights)
print("mean height:", heights.mean())
print("standart deviation:", heights.std())
print("minimum :", heights.min())
print("maximum : ", heights.max())
print("25th percentile:", np.percentile(heights, 25))
print("median :", np.median(heights))
print("75th percentile:", np.percentile(heights, 75))

plt.hist(heights)
plt.title("height distribution of president")
plt.xlabel('height(cm)')
plt.ylabel('number')
plt.show()