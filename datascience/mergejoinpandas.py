import numpy as np 
import pandas as pd 

pop = pd.read_csv('data/state-population.csv')
areas = pd.read_csv('data/state-areas.csv')
abbrevs = pd.read_csv('data/state-abbrevs.csv')

# print(pop.head())
# print(areas.head())
# print(abbrevs.head())

merged = pd.merge(pop, abbrevs, how='outer', left_on='state/region', right_on='abbreviation')
merged = merged.drop('abbreviation', 1)
y = merged.head()
print(y)
cm = merged.isnull().any()
print('data mismatch check :')
print(cm)