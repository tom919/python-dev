import seaborn as sns
import pandas as pd
import numpy as np 

titanic = pd.read_csv('data/titanic.csv')
ms = titanic.groupby('sex')[['survived']].mean()
mspv = titanic.pivot_table('survived', index='sex', columns='class')

age = pd.cut(titanic['age'],[0,18,80])
