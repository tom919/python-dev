import numpy as np 

x = np.array([1,2,3,4])
y = np.array([5,6,7,8])
z = np.concatenate([x,y])
print("z :", z)

v = np.array([
    [1,2,3,4],
    [5,6,7,8]
])

vs = np.concatenate([v, v])
print("v : ", v)

z = np.array([9,8,7,6])
vsz = np.vstack([z, vs])
print("vsz: ", vsz)