import numpy as np 
import pandas as pd 

vals1 = np.array([1, None, 3, 4])
print(vals1)
data = pd.Series([1, np.nan, 'hello', None])
datacheck = data.isnull()
print(datacheck)
datafilter = data.dropna()
print(datafilter)
datamanipulate = data.fillna(0)
print(datamanipulate)
datamanipulateforward = data.fillna(method='ffill')
print(datamanipulateforward)
datamanipulatebackward = data.fillna(method='bfill')
print(datamanipulatebackward)