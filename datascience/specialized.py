from scipy import special

x = [1,5,10]
print("gamma x :", special.gamma(x))
print("gamma ln x:", special.gammaln(x))
print("beta x 2", special.beta(x,2))