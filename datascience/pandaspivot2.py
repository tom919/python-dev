import seaborn as sns
import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt

births = pd.read_csv('data/births.csv')
births['decade'] = 10*(births['year'] //10)
br = births.pivot_table('births', index='decade', columns='gender', aggfunc='sum')
quartiles = np.percentile(births['births'], [25, 50, 75])
mu = quartiles[1]
sig = 0.74*(quartiles[2]-quartiles[0])

sns.set()
births.pivot_table('births', index='year', columns='gender', aggfunc='sum').plot()
plt.ylabel('total births per year')
plt.show()