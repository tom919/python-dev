def smallest_item(xs):
    return min(xs)

assert smallest_item([10,20, 5, 40]) == 5
assert smallest_item([1,0,-1,2]) == -1