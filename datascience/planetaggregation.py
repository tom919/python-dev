import seaborn as sns
import pandas as pd 

planets = pd.read_csv('data/planets.csv')

# print(planets.shape)
# print(planets.head())
print(planets.dropna().describe())
print("unstack planet:")
print(planets.groupby('method')['year'].describe().unstack())