import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt 
import seaborn; seaborn.set()


# use pandas to extract
rainfall = pd.read_csv('data/seattlerain2014.csv')['PRCP'].values
inches = rainfall / 254 #1/10 mm - inches
print(inches.shape)
plt.hist(inches, 40)
plt.show()