import matplotlib.pyplot as plt 
import seaborn; seaborn.set()
import numpy as np

X = np.random.rand(10,2)
#each pair of point, compute differences
differences = X[:, np.newaxis,:] - X[np.newaxis,:,:]
differences.shape
#square coordinate differences
sq_differences = differences**2
sq_differences.shape
#sum the coordinate differences to get squared distance
dist_sq = sq_differences.sum(-1)
dist_sq.shape
dist_sq.diagonal()
nearest = np.argsort(dist_sq, axis = 1)
K = 2        
nearest_partition = np.argpartition(dist_sq, K + 1, axis=1)

plt.scatter(X[:, 0], X[:, 1], s=100)
# draw lines from each point to its two nearest neighbors        
K = 2        
for i in range(X.shape[0]):            
    for j in nearest_partition[i, :K+1]:                
        # plot a line from X[i] to X[j]               
        # use some zip magic to make it happen:                
        plt.plot(*zip(X[j], X[i]), color='black')
plt.show()