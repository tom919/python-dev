import numpy as np 
import pandas as pd 

rng = np.random.RandomState(0)
df = pd.DataFrame({'key': ['A', 'B', 'C', 'A', 'B', 'C'],
                   'data1': range(6),
                   'data2': rng.randint(0, 10, 6)},
                   columns = ['key', 'data1', 'data2'])

print(df)

y = df.groupby('key').aggregate(['min', np.median, max])
print("aggregation : ")
print(y)