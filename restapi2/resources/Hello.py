from flask_restful import Resource
from flask_jwt_extended import (create_access_token, create_refresh_token,get_jwt_identity, jwt_required, get_raw_jwt, fresh_jwt_required, jwt_refresh_token_required)

class Hello(Resource):
    @jwt_required
    def get(self):
        return {"message": "Hello, World!"}