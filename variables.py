x = 5 #int variable
y = "john" # string variable
print(x)
print(y)
print(type(y))

z = """ test multiple line
 of text."""

print(z)

text1 = "Hello, World"

print(text1[0])
print(text1[2:5])
print(text1[-7:-1])
print(text1.lower())
print(text1.replace("H","J"))

j = "orld" in text1
print(j)

k = "qwer" not in text1
print(k)

quantity = 5
item = 10
price = 15.25

order = "I want buy {} pieces of {} for {} USD"
print(order.format(quantity, item, price))