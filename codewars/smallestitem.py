def sum_smallest(n):  
    n.sort()
    return n[0]+n[1]

data = [100,33,21,77,65,14,81,96]
print("first formula : ", sum_smallest(data))

def sum_smallest2(n):
    return sum(sorted(n)[:2])

print("second formula : ", sum_smallest2(data))