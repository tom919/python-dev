def bmi(weight, height):
    res = weight / (height**2)
    if  18.5 <= res <= 25.0 :
        return "Normal"
    elif 25.0 <= res <= 30.0:
        return "Overweight"
    elif res <= 18.5:
        return "Underweight"
    else :
        return "Obese"


print(bmi(60, 1.68))