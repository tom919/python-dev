import json

# some JSON

x = '{"name":"abu jandal", "age":"40", "city":"solo"}'

# parse JSON
y = json.loads(x)

#print result

print(y["age"])


# a Python object (dict):
x = {
  "name": "John",
  "age": 30,
  "city": "New York"
}

# convert into JSON:
y = json.dumps(x)

# the result is a JSON string:
print(y) 